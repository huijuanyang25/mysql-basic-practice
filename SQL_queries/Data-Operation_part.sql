USE student_management_sys_huijuanyang;
ALTER TABLE teacher RENAME COLUMN gender TO name;
ALTER TABLE student RENAME COLUMN gender TO name;
INSERT INTO student VALUES ('1', '张三', '18', '男', '15342349123'), ('2', '李四', '20', '女', '13884356789'),
 ('3', '王五', '26', '女', '13884359789'), ('4', '赵六', '16', '女', '13884334789');
INSERT INTO teacher VALUES ('1', '刘老师', '38', '男', 1, '15343349123'), ('2', '李老师', '40', '女', 2, '13885356789'),
 ('3', '张老师', '56', '女', 3, '18784359789');
INSERT INTO subject VALUES ('1', '语文', '40', ' '), ('2', '数学', '30', ' '), ('3', '英语', '45', ' ');
INSERT INTO exam VALUES ('1', '期末考试', '1'), ('2', '期末考试', '2'), ('3', '中期考试', '3'), ('4', '中期考试', '1');
INSERT INTO score VALUES (1, 1, 1, 80), (2, 1, 2, 60), (3, 1, 3, 70), (4, 1, 4, 60.5), (5, 2, 1, 40), (6, 2, 2, 30), (7, 2, 3, 20),
 (8, 2, 4, 55), (9, 3, 1, 48), (10, 3, 2, 39), (11, 3, 3, 70), (12, 3, 4, 95), (13, 4, 1, 78), (14, 4, 2, 88), (15, 4, 3, 0), (16, 4, 4, 100);

SET sql_safe_updates = OFF;
UPDATE student SET phone = '13456789876' WHERE name = '赵六';
UPDATE subject SET description = '该课程是本校特殊课程，由非常资深的刘老师授课。' WHERE name = '语文';
UPDATE score SET score = 98 WHERE student_id = (SELECT id FROM student WHERE name = '赵六') 
 AND exam_id = (SELECT id FROM exam WHERE name = '中期考试' 
 AND subject_id = (SELECT id FROM subject WHERE name = '语文'));

SELECT * FROM student;
SELECT * FROM student WHERE age >= 18;
DELETE FROM score WHERE student_id = (SELECT id FROM student WHERE name = '赵六') 
 AND exam_id = (SELECT id FROM exam WHERE name = '中期考试' 
 AND subject_id = (SELECT id FROM subject WHERE name = '语文'));