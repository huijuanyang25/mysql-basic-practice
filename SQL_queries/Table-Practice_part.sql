CREATE DATABASE student_management_sys_huijuanyang DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
USE student_management_sys_huijuanyang;

CREATE TABLE student (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
`name` VARCHAR(100) NOT NULL,
`age` INT NOT NULL CHECK (age > 0),
`sex` VARCHAR(2) NOT NULL CHECK (sex = '男' OR sex = '女'),
`phone` VARCHAR(11) NOT NULL CHECK (length(phone) = 11)
)ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE TABLE teacher (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
`name` VARCHAR(100) NOT NULL,
`age` INT NOT NULL CHECK (age > 0),
`sex` VARCHAR(2) NOT NULL CHECK (sex = '男' OR sex = '女'),
`subject_id` INT NOT NULL
)ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE TABLE subject (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
`name` VARCHAR(100) NOT NULL,
`period` INT NOT NULL,
`description` TEXT
)ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE TABLE exam (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
`name` VARCHAR(100) NOT NULL,
`subject_id` INT NOT NULL
)ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE TABLE score (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
`student_id` INT NOT NULL,
`exam_id` INT NOT NULL,
`score` FLOAT NOT NULL
)ENGINE = InnoDB DEFAULT CHARSET = utf8;

ALTER TABLE teacher ADD COLUMN phone CHAR(11) NOT NULL CHECK (length(phone) = 11);
ALTER TABLE score MODIFY score FLOAT NOT NULL CHECK (score >= 0 AND score <= 100);
ALTER TABLE teacher RENAME COLUMN name TO gender;
ALTER TABLE student RENAME COLUMN name TO gender;
DROP TABLE score;

CREATE TABLE score (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
`student_id` INT NOT NULL,
`exam_id` INT NOT NULL,
`score` FLOAT NOT NULL CHECK (score >= 0 AND score <= 100)
)ENGINE = InnoDB DEFAULT CHARSET = utf8;
