USE student_management_sys_huijuanyang;
SELECT * FROM score WHERE exam_id = (SELECT id FROM exam WHERE name = '期末考试' 
 AND subject_id = (SELECT id FROM subject WHERE name = '语文'));
SELECT * FROM student WHERE id IN (SELECT student_id FROM score WHERE exam_id = (SELECT id FROM exam WHERE name = '中期考试'
 AND subject_id = (SELECT id FROM subject WHERE name = '语文')));
SELECT * FROM student WHERE id IN (SELECT student_id FROM score WHERE exam_id = (SELECT id FROM exam WHERE name = '期末考试'
 AND subject_id = (SELECT id FROM subject WHERE name = '语文')) AND score >= 60);
SELECT * FROM student WHERE id IN (SELECT student_id FROM score WHERE exam_id = (SELECT id FROM exam WHERE name = '期末考试'
 AND subject_id = (SELECT subject_id FROM teacher WHERE name = '李老师')) AND score >= 60);
SELECT AVG(score) AS average_score FROM score WHERE exam_id = (SELECT id FROM exam WHERE name = '期末考试'
 AND subject_id = (SELECT subject_id FROM teacher WHERE name = '李老师'));
SELECT MAX(score) AS highest_score FROM score WHERE exam_id = (SELECT id FROM exam 
 WHERE subject_id = (SELECT subject_id FROM teacher WHERE name = '李老师'));
SELECT MIN(score) AS lowest_score FROM score WHERE exam_id = (SELECT id FROM exam 
 WHERE subject_id = (SELECT subject_id FROM teacher WHERE name = '李老师'));
SELECT SUM(score) AS total_score FROM score WHERE exam_id IN (SELECT id FROM exam WHERE name = '期末考试')
 AND student_id = (SELECT id FROM student WHERE name = '张三');
UPDATE score SET score = 88 WHERE exam_id = (SELECT id FROM exam WHERE name = '期末考试' 
 AND subject_id = (SELECT id FROM subject WHERE name = '语文'))
 AND student_id = (SELECT id FROM student WHERE name = '张三');
SELECT COUNT(DISTINCT(student_id)) AS num_cust FROM score WHERE exam_id IN (SELECT id FROM exam WHERE name = '中期考试');
